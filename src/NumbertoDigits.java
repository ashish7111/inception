import java.util.ArrayList;
import java.util.Scanner;

public class NumbertoDigits {

	public static void main(String[] args) {
		System.out.println("Enter a number");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();	
		
		String s = Integer.toString(n);
		String [] list = s.split("");
		
		for (String s1: list) {
			System.out.print( s1+",  ");
		}

		System.out.println();
		System.out.println(s);
		
	}

}
