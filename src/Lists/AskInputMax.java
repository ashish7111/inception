package Lists;

import java.util.Scanner;

public class AskInputMax {

	public static void main(String[] args) {
		System.out.println("How many numbers do you want in your list?");
		Scanner s = new Scanner(System.in);
		int elem = s.nextInt();

		int[] list = new int[elem];
// Take input of scanners
		for (int i = 0; i < list.length; i++) {
			System.out.print("Enter a number:  \t");
			Scanner s1 = new Scanner(System.in);
			int n = s1.nextInt();
			list[i] = n;
		}
		System.out.println();

//Print the input list
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + "\t");
		}
		System.out.println();
//Reverse the list
		for (int i = 0; i <= list.length - 1; i++) {
			for (int j = i; j < list.length; j++) {

				int temp = list[j];
				list[j] = list[i];
				list[i] = temp;

			}

		}

		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + "\t");
		}
		System.out.println();
//List in ascending order
		for (int i = 0; i <= list.length - 1; i++) {
			for (int j = i; j < list.length; j++) {
				if (list[i] > list[j]) {
					int temp = list[j];
					list[j] = list[i];
					list[i] = temp;

				}

			}

		}
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + "\t");
		}
		
		
//List in descending order
		for (int i = 0; i <= list.length - 1; i++) {
			for (int j = i; j < list.length; j++) {
				if (list[i] < list[j]) {
					int temp = list[j];
					list[j] = list[i];
					list[i] = temp;

				}

			}

		}
		System.out.println();
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + "\t");
		}
		System.out.println();
		//max in the list
		System.out.println("Max is " + list[0]);
		System.out.println();
		//Min in the list
		System.out.println("Min is " + list[list.length - 1]);
	}

}
