package Lists;

import java.util.Scanner;

public class StringPalindrome {
	public static void main(String[] args) {
		Scanner check = new Scanner (System.in);
		System.out.println("Please Enter a String : ");
		String s1 = check.nextLine();
		
		String [] list = s1.split("");
		String s2 ="";
		
		for (int i =0; i<list.length-1; i++) {
			for (int j =i; j<list.length; j++) {
				String temp = list[j];
				list[j] = list [i];
				list[i] = temp;
			}
		}
		for (int i=0; i<list.length; i++) {
			s2+= list[i];
		}
		if (s1.equals(s2)) {
			System.out.println("Palindrome");
		}
		else System.out.println("Not a palindrome");
	}

}
