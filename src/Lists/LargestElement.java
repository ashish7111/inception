package Lists;

public class LargestElement {

	public static void main(String[] args) {

		int[] list = { 10, 23,12, 43 };

		for (int i = 0; i < list.length - 1; i++) {
			for (int j = i; j < list.length; j++) {
				if (list[j] > list[i]) {
					
					int temp = list[j];
					list[j] = list[i];
					list[i] = temp;

				}
			}
		}
		System.out.println(list[0]);
	}

}

