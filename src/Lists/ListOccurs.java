package Lists;

import java.util.Scanner;

public class ListOccurs {
	public static void main(String[] args) {
		int [] list = {1, 23, 16, 10, 101, 1000};
		
		System.out.println("Enter a number you think is in the list :");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		boolean test = false;
		
		for (int i=0; i <list.length-1; i++) {
			if (list [i] == n) {
				test = true;
				break;
			}
			else test = false;
			
		}
		
		if (test== true) {
			System.out.println("The number is in the list");
		}
		else System.out.println("The number is not in the list");
	}

}
