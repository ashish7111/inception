import java.util.Scanner;

public class Numbers {
	public static void main(String[] args) {
		System.out.println("Please Enter a number whose sum you would like:");
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int sum = 0;
		for(int i =0; i<=n; i++) {
			if (i%3 == 0 || i%5== 0) {
			  sum+=i;				
			}
		}
		
		System.out.println("The Sum is: " +sum);
	}
}
