package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer {

	private String customerName;
	private ArrayList<Account> accounts = new ArrayList<>();
	private String address;
	
	public Customer(String CustomerName, ArrayList<Account> accounts, String address) {
		this.customerName = customerName;
		this.accounts = accounts;
		this.address = address;
		
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	
	public ArrayList<Account> printCustomerAccounts(ArrayList<Account> accounts) {

		return accounts;
	}
}
