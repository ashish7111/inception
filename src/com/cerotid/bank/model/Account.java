package com.cerotid.bank.model;

public class Account {
	
	private String accountType ;
	
	public Account (String accountType) {
		this.accountType = accountType;
	}
	
	public void printAccountInfo(String accountType) {
		this.accountType = accountType;
		System.out.println(accountType);
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	

}
