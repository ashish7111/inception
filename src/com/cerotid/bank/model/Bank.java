package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {
	
	private String bankName;
	private ArrayList<Customer> customers = new ArrayList<>();
	
	public Bank (String bankName, ArrayList<Customer> customers) {
		this.bankName = bankName;
		this.customers = customers;
	}
	
	public String getBankName() {
		return bankName;
	}
	
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public ArrayList<Customer> getCustomers(){
		return customers;
	}
	
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	
	public void printBankName(String bankName) {
		this.bankName = bankName;
		System.out.println(bankName);
		
	}
	
//	public void printBankDetails(String BankName, ArrayList<Customer> customers) {
//		
//				
//		
//	}

}
