package Files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class ReadCustomers {
	public static void main(String[] args) throws FileNotFoundException {
		String fName = "Customers.txt";
		String line;
		ArrayList<String> aList = new ArrayList<>();
		
		try {
			FileReader fr = new FileReader("Customers.txt");
			BufferedReader br = new BufferedReader(fr);
			
			while ((line = br.readLine()) != null) {
				aList.add(line);
			}
			
			Collections.sort(aList);
			
			for(String s: aList) {
				System.out.println(s);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}

}
